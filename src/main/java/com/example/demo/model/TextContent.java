package com.example.demo.model;

import lombok.Getter;

import javax.persistence.*;
import java.util.Objects;
import java.util.Set;

@Entity
@Table(name = "content")
@Getter
public class TextContent {

    @Id
    @GeneratedValue
    private Long id;

    private String title;

    private String content;

    @ManyToMany(mappedBy = "purchasedContent", fetch = FetchType.LAZY)
    private Set<UserAccount> userAccounts;

    @Override
    public boolean equals(Object o) {
        if (this == o) {
            return true;
        }
        if (o == null || getClass() != o.getClass()) {
            return false;
        }

        TextContent that = (TextContent)o;

        if (!Objects.equals(id, that.id)) {
            return false;
        }
        if (!Objects.equals(title, that.title)) {
            return false;
        }
        return Objects.equals(content, that.content);

    }

    @Override
    public int hashCode() {
        int result = id != null ? id.hashCode() : 0;
        result = 31 * result + (title != null ? title.hashCode() : 0);
        result = 31 * result + (content != null ? content.hashCode() : 0);
        return result;
    }
}

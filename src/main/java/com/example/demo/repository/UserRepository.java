package com.example.demo.repository;

import com.example.demo.model.UserAccount;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;

@Component
@RequiredArgsConstructor
public class UserRepository {

    private final EntityManager entityManager;

    public UserAccount getById(Long id) {
        return entityManager.find(UserAccount.class, id);
    }

    public void create(UserAccount userAccount) {
        entityManager.persist(userAccount);
    }

    public UserAccount findOneByMsisdn(String msisdn) {
        return entityManager.createQuery("select u from UserAccount u where u.msisdn = :msisdn", UserAccount.class)
                            .setParameter("msisdn", msisdn)
                            .getResultStream()
                            .findFirst().orElse(null);
    }

    public void update(UserAccount user) {
        entityManager.merge(user);
    }
}

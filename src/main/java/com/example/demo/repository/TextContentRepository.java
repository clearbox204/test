package com.example.demo.repository;

import com.example.demo.model.TextContent;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;

import javax.persistence.EntityManager;
import java.util.List;

@Component
@RequiredArgsConstructor
public class TextContentRepository {

    private final EntityManager entityManager;

    public List<TextContent> findAll() {
        return entityManager.createQuery("select c from TextContent c", TextContent.class).getResultList();
    }
}

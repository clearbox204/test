package com.example.demo;

import com.example.demo.service.Server;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.CommandLineRunner;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

import java.util.Scanner;


@SpringBootApplication
public class ServerApplication implements CommandLineRunner {

    @Autowired
    private Server server;

    public static void main(String[] args) {
        SpringApplication.run(ServerApplication.class, args);
    }

    @Override
    public void run(String... args) {

        Scanner consoleReader = new Scanner(System.in);
        System.out.println("Enter server port: ");
        int port = consoleReader.nextInt();

        server.start(port);
    }
}

package com.example.demo.service;

import lombok.RequiredArgsConstructor;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.ApplicationContext;
import org.springframework.stereotype.Service;

import java.io.IOException;
import java.net.ServerSocket;

@Slf4j
@Setter
@Service
@RequiredArgsConstructor
public class Server {

    private final UserService userService;
    private final TextContentService textContentService;
    private final ApplicationContext applicationContext;

    public void start(int port) {
        try (ServerSocket serverSocket = new ServerSocket(port)) {
            log.info(String.format("Server started. Listening port: %d", port));
            while (true) {
                ClientHandler clientHandler = (ClientHandler)applicationContext.getBean("clientHandler");
                clientHandler.setClientSocket(serverSocket.accept());
                clientHandler.start();
            }
        } catch (IOException e) {
            throw new RuntimeException("Server error", e);
        }
    }
}
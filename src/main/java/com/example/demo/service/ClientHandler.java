package com.example.demo.service;

import com.example.demo.model.TextContent;
import com.example.demo.model.UserAccount;
import lombok.Setter;
import lombok.extern.slf4j.Slf4j;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Set;

import static java.lang.String.format;
import static java.util.Objects.isNull;

@Slf4j
public class ClientHandler extends Thread {

    private static final String DELIMITER = ";;";
    private static final String ONLY_DIGITS_REGEX = "\\d+";
    private static final String ONE_DIGIT_REGEX = "\\d";

    @Setter
    private Socket clientSocket;
    private UserService userService;
    private TextContentService textContentService;

    public ClientHandler(UserService userService, TextContentService textContentService) {
        this.userService = userService;
        this.textContentService = textContentService;
    }

    public void run() {
        try (PrintWriter out = new PrintWriter(clientSocket.getOutputStream(), true);
             BufferedReader in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()))) {
            log.info("Client accepted");
            out.println("Welcome. Input your msisdn");
            outputDelimiter(out);

            String msisdn = in.readLine();
            UserAccount user = getUserAccount(out, in, msisdn);

            out.println(format("Hello %s", user.getName()));

            while (true) {
                printMenu(out);
                String menuOption = in.readLine();
                if (!menuOption.matches(ONE_DIGIT_REGEX)) {
                    out.println("Wrong input. Enter menu item number");
                    continue;
                }
                switch (menuOption) {
                    case "1":
                        processPurchase(out, in, user.getId());
                        continue;
                    case "2":
                        showPurchasedContent(out, user.getId());
                        continue;
                    default:
                        out.println("Wrong input. Enter menu item number");

                }
            }

        } catch (IOException e) {
            throw new RuntimeException("Client handler error", e);
        } finally {
            try {
                clientSocket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }

    private void printMenu(PrintWriter out) {
        out.println("Choose menu option:");
        out.println("1.Purchase content");
        out.println("2.Show purchased content");
        outputDelimiter(out);
    }

    private UserAccount getUserAccount(PrintWriter out, BufferedReader in, String msisdn) throws IOException {
        UserAccount user = userService.getUserByMsisdn(msisdn);
        while (isNull(user)) {
            out.println(format("User with msisdn %s not found. Input another value", msisdn));
            outputDelimiter(out);
            msisdn = in.readLine();
            user = userService.getUserByMsisdn(msisdn);
        }
        return user;
    }

    private void processPurchase(PrintWriter out, BufferedReader in, Long userId) throws IOException {
        boolean exit = false;
        Set<TextContent> allContent = textContentService.findAll();
        while (!exit) {
            printAvailableContent(out, allContent);

            String contentId = in.readLine();
            if (!contentId.matches(ONLY_DIGITS_REGEX)) {
                out.println("Wrong input. Enter menu item number");
                outputDelimiter(out);
                continue;
            }
            long id = Long.parseLong(contentId);
            TextContent contentToPurchase = allContent.stream()
                                                      .filter(content -> content.getId().equals(id))
                                                      .findFirst().orElse(null);
            if (contentToPurchase == null) {
                out.println("Wrong input. Enter menu item number");
                outputDelimiter(out);
                continue;
            }
            userService.purchaseContent(userId, contentToPurchase);
            exit = true;
        }
    }

    private void printAvailableContent(PrintWriter out, Set<TextContent> allContent) {
        out.println("Available content:");
        showContentList(out, allContent);
        out.println("Enter id to purchase");
        outputDelimiter(out);
    }

    private void showPurchasedContent(PrintWriter out, Long userId) {
        showContentList(out, userService.getPurchasedContent(userId));
    }

    private void showContentList(PrintWriter out, Set<TextContent> textContentList) {
        textContentList.forEach(content -> out.println(format("id=%d title=%s", content.getId(), content.getTitle())));
    }

    private void outputDelimiter(PrintWriter out) {
        out.println(DELIMITER);
    }
}
package com.example.demo.service;

import com.example.demo.model.TextContent;
import com.example.demo.repository.TextContentRepository;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.util.HashSet;
import java.util.Set;

@Service
@RequiredArgsConstructor
public class TextContentService {

    private final TextContentRepository contentRepository;

    public Set<TextContent> findAll() {
        return new HashSet<>(contentRepository.findAll());
    }

}

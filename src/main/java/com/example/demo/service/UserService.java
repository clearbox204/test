package com.example.demo.service;

import com.example.demo.model.TextContent;
import com.example.demo.model.UserAccount;
import com.example.demo.repository.UserRepository;
import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;
import java.util.HashSet;
import java.util.Set;

import static java.lang.String.format;
import static java.util.Objects.isNull;

@Slf4j
@Service
@Transactional
@RequiredArgsConstructor
public class UserService {

    private final UserRepository userRepository;

    public Set<TextContent> getPurchasedContent(Long userId) {
        UserAccount user = userRepository.getById(userId);
        if (isNull(user)) {
            log.warn(format("User not found. id=%d", userId));
            return new HashSet<>();
        }
        return user.getPurchasedContent();
    }

    public UserAccount getUserByMsisdn(String msisdn) {
        return userRepository.findOneByMsisdn(msisdn);
    }

    public void purchaseContent(Long userId, TextContent contentToPurchase) {
        UserAccount user = userRepository.getById(userId);
        if (isNull(user)) {
            log.warn(format("User not found. id=%d", userId));
            return;
        }
        user.getPurchasedContent().add(contentToPurchase);
        userRepository.update(user);
    }
}

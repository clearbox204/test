package com.example.demo.configuration;

import com.example.demo.service.ClientHandler;
import com.example.demo.service.TextContentService;
import com.example.demo.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;


@Configuration
public class BeanConfig {

    @Autowired
    private UserService userService;
    @Autowired
    private TextContentService textContentService;

    @Bean
    public ClientHandler clientHandler() {
        return new ClientHandler(userService, textContentService);
    }

}
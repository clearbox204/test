import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.Scanner;

public class Client {

    private static final String LOCALHOST = "127.0.0.1";
    private static final String DELIMITER = ";;";

    public static void main(String[] args) {

        Scanner consoleReader = new Scanner(System.in);
        System.out.println("Enter server port: ");
        int port = consoleReader.nextInt();

        try (Socket clientSocket = new Socket(LOCALHOST, port);
             PrintWriter out = new PrintWriter(clientSocket.getOutputStream(), true);
             BufferedReader in = new BufferedReader(new InputStreamReader(clientSocket.getInputStream()))) {

            Boolean clientRunning = true;

            while (clientRunning) {
                processServerResponse(in);
                clientRunning = processUserInput(consoleReader, out);
            }
        } catch (IOException e) {
            throw new RuntimeException("Client error", e);
        }
    }

    private static Boolean processUserInput(Scanner consoleReader, PrintWriter out) {
        if (consoleReader.hasNext()) {
            String userInput = consoleReader.next();
            if (userInput.equals("exit")) {
                return false;
            }
            out.println(userInput);
        }
        return true;
    }

    private static void processServerResponse(BufferedReader in) throws IOException {
        String line = in.readLine();
        while (line != null && !line.isEmpty()) {
            System.out.println(line);
            line = in.readLine();
            if (line.equals(DELIMITER)) {
                break;
            }
        }
    }

}
